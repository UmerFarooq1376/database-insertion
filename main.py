import psycopg2
from datetime import *
import psutil
import matplotlib.pyplot as plt
import xlwt
import threading
##==============main=================

TIME_IN_MINUTES=0.1*(60)

ram_percentage=[]
ram_usage_gb=[]
ram_time_plot=[]
cpu_usage=[]
count_data_insertion=0
Exit_flag=False
##===================================
def create_Table(database_object_cursor,column_list_temp):
    
    column_list_temp=' TEXT ,'.join(column_list_temp)
    column_list_temp=column_list_temp + " TEXT"
    database_object_cursor.execute(f"CREATE TABLE person({column_list_temp})")

def insert_values(database_object_cursor,value_list,column_list_temp):
    value_list=tuple(value_list)
    column_list_temp=','.join(column_list_temp)
    val=['%s']*31
    val=','.join(val)
    query=str("""INSERT INTO person({0}) VALUES({1})""".format(column_list_temp,val))
    database_object_cursor.execute(query,(value_list)) 

def get_namelist_and_columnlist(a):
    column_list=[]
    name_list=[]
    time_list=[]
    for i in a:
    # print(i['n'])
        try:
            temp_name=((i['bn']))
            temp_name=temp_name.split(":")[0:2]
            temp_name=[temp_name[1].split("-")[0],temp_name[1].split("-")[3]]+(temp_name[0:1])
            # temp_name.append()
            print((temp_name))
            name_list.append(str(i['vs']))#
            column_list.append(i['n'])
        except:
            try:
                name_list.append(str(i['v']))#
                column_list.append(i['n'])
                time_list.append(str(i['ut']))
            except:
                print("Exception")
    name_list=temp_name+time_list[len(time_list)-1:len(time_list)]+name_list[1:]
    column_list[1:1]=("gateway","siteid","time")
    return name_list,column_list

def plot_usage(ram_usage_gb,ram_percentage,cpu_usage,ram_time_plot):
    plt.figure(1)
    plt.plot(ram_time_plot, ram_usage_gb)
    plt.title('RAM usage in GBs')
    plt.figure(2)
    plt.plot(ram_time_plot, ram_percentage)
    plt.title('RAM usage in %')
    plt.figure(3)
    plt.plot(ram_time_plot, cpu_usage)
    plt.title('CPU usage')
    plt.show()

def create_report(count_data_insertion):
    # create a new XLSX workbook
    workbook = xlwt.Workbook() 
    sheet = workbook.add_sheet("Sheet Name")
    # Specifying style
    style = xlwt.easyxf('font: bold 1')   
    # Specifying column
    count=0
    sheet.write(count,0,"Time", style)
    sheet.write(count,1,"Ram percentage", style)
    sheet.write(count,2,"Ram Usage in GBs", style)
    sheet.write(count,3,"CPU Usage", style)
    sheet.write(count,4,"Data Insertion/sec")
    sheet.write(count,5,"Total Writes")
    sheet.write(count,6,"Misses")

    sheet.write(1,4,count_data_insertion/TIME_IN_MINUTES)
    sheet.write(1,5,len(json_list), style)
    sheet.write(1,6,len(json_list)-count_data_insertion, style) 
    for i in ram_time_plot:
        count=count+1
        sheet.write(count,0, str(i), style)
    count=0
    for i in ram_percentage:
        count=count+1
        sheet.write(count,1, str(i), style)
    count=0
    for i in ram_usage_gb:
        count=count+1
        sheet.write(count,2, str(i), style)
    count=0
    for i in cpu_usage:
        count=count+1
        sheet.write(count,3, str(i), style)


    workbook.save("sample.xls")

def system_usage_report(a):
    ram_time_plot.append(a)
    per_cpu = psutil.cpu_percent(percpu=True)
    cpu_usage.append(psutil.cpu_percent())
    ram_percentage.append(psutil.virtual_memory()[2])
    ram_usage_gb.append(psutil.virtual_memory()[3]/1000000000)

def create_connection():
    connection = psycopg2.connect(

        host="localhost",
        database="test",
        user="postgres",
        password="root"
    )

    connection.autocommit=True
    print("Success")
    return connection



json_list=[[    

{"bn":"ISB_LAB_237:energy_meter-9--A0000000092ba50b0:","n":"hw_code","vs":"energy_meter-9--0000000092ba50b0"},
{"n": "power_factor_c", "t": 1647010680, "ut": 1647010678.948358, "u": "PF", "v": -0.75}, 
{"n": "current_C", "t": 1647010680, "ut": 1647010678.9676173, "u": "A", "v": 23.71}, 
{"n": "voltage_BC", "t": 1647010680, "ut": 1647010678.960211, "u": "V", "v": 0.09}, 
{"n": "power_factor_a", "t": 1647010680, "ut": 1647010678.9464207, "u": "PF", "v": -0.79}, 
{"n": "voltage_C", "t": 1647010680, "ut": 1647010678.956164, "u": "V", "v": 239.72}, 
{"n": "active_power_b", "t": 1647010680, "ut": 1647010678.9252253, "u": "kW", "v": 4.83}, 
{"n": "apparent_power_b", "t": 1647010680, "ut": 1647010678.9339702, "u": "kVA", "v": 5.93}, 
{"n": "active_power_c", "t": 1647010680, "ut": 1647010678.927481, "u": "kW", "v": 4.8}, 
{"n": "voltage_avg", "t": 1647010680, "ut": 1647010678.937958, "u": "A", "v": 239.7}, 
{"n": "voltage_AC", "t": 1647010680, "ut": 1647010678.962584, "u": "V", "v": 0.02},
{"n": "voltage_A", "t": 1647010680, "ut": 1647010678.9521866, "u": "V", "v": 239.75}, 
{"n": "power_factor_total", "t": 1647010680, "ut": 1647010678.944064, "u": "PF", "v": -0.79}, 
{"n": "current_B", "t": 1647010680, "ut": 1647010678.966107, "u": "A", "v": 23.95},
{"n": "apparent_power_a", "t": 1647010680, "ut": 1647010678.9318821, "u": "kVA", "v": 24.18},
{"n": "energy_phase_b", "t": 1647010680, "ut": 1647010678.9726903, "u": "kWh", "v": 19932.34},
{"n": "active_power_a", "t": 1647010680, "ut": 1647010678.9228728, "u": "a", "v": 19.59}, 
{"n": "energy_phase_a", "t": 1647010680, "ut": 1647010678.970977, "u": "kWh", "v": 81121.3}, 
{"n": "current_A", "t": 1647010680, "ut": 1647010678.9643526, "u": "A", "v": 97.63}, 
{"n": "average_voltage_phase_to_phase", "t": 1647010680, "ut": 1647010678.9398596, "u": "V", "v": 0.07}, 
{"n": "total_energy", "t": 1647010680, "ut": 1647010678.9693985, "u": "kWh", "v": 120866.31},
{"n": "total_instantaneous_real_power", "t": 1647010680, "ut": 1647010678.9198678, "u": "kW", "v": 29.22}, 
{"n": "energy_phase_c", "t": 1647010680, "ut": 1647010678.9745023, "u": "kWh", "v": 19812.67}, 
{"n": "average_current", "t": 1647010680, "ut": 1647010678.9420502, "u": "A", "v": 48.43}, 
{"n": "apparent_power_c", "t": 1647010680, "ut": 1647010678.9360523, "u": "kVA", "v": 5.88},
{"n": "voltage_B", "t": 1647010680, "ut": 1647010678.9540834, "u": "V", "v": 239.64},
{"n": "frequency", "t": 1647010680, "ut": 1647010678.9502673, "u": "Hz", "v": 49.87},
{"n": "voltage_AB", "t": 1647010680, "ut": 1647010678.9581149, "u": "V", "v": 0.11}
],[    

{"bn":"ISB_LAB_255:energy_meter-9--A0000000092ba50b0:","n":"hw_code","vs":"energy_meter-9--0000000092ba50b0"},
{"n": "power_factor_c", "t": 1647010680, "ut": 1647010678.948358, "u": "PF", "v": -0.75}, 
{"n": "current_C", "t": 1647010680, "ut": 1647010678.9676173, "u": "A", "v": 23.71}, 
{"n": "voltage_BC", "t": 1647010680, "ut": 1647010678.960211, "u": "V", "v": 0.09}, 
{"n": "power_factor_a", "t": 1647010680, "ut": 1647010678.9464207, "u": "PF", "v": -0.79}, 
{"n": "voltage_C", "t": 1647010680, "ut": 1647010678.956164, "u": "V", "v": 239.72}, 
{"n": "active_power_b", "t": 1647010680, "ut": 1647010678.9252253, "u": "kW", "v": 4.83}, 
{"n": "apparent_power_b", "t": 1647010680, "ut": 1647010678.9339702, "u": "kVA", "v": 5.93}, 
{"n": "active_power_c", "t": 1647010680, "ut": 1647010678.927481, "u": "kW", "v": 4.8}, 
{"n": "voltage_avg", "t": 1647010680, "ut": 1647010678.937958, "u": "A", "v": 239.7}, 
{"n": "voltage_AC", "t": 1647010680, "ut": 1647010678.962584, "u": "V", "v": 0.02},
{"n": "voltage_A", "t": 1647010680, "ut": 1647010678.9521866, "u": "V", "v": 239.75}, 
{"n": "power_factor_total", "t": 1647010680, "ut": 1647010678.944064, "u": "PF", "v": -0.79}, 
{"n": "current_B", "t": 1647010680, "ut": 1647010678.966107, "u": "A", "v": 23.95},
{"n": "apparent_power_a", "t": 1647010680, "ut": 1647010678.9318821, "u": "kVA", "v": 24.18},
{"n": "energy_phase_b", "t": 1647010680, "ut": 1647010678.9726903, "u": "kWh", "v": 19932.34},
{"n": "active_power_a", "t": 1647010680, "ut": 1647010678.9228728, "u": "a", "v": 19.59}, 
{"n": "energy_phase_a", "t": 1647010680, "ut": 1647010678.970977, "u": "kWh", "v": 81121.3}, 
{"n": "current_A", "t": 1647010680, "ut": 1647010678.9643526, "u": "A", "v": 97.63}, 
{"n": "average_voltage_phase_to_phase", "t": 1647010680, "ut": 1647010678.9398596, "u": "V", "v": 0.07}, 
{"n": "total_energy", "t": 1647010680, "ut": 1647010678.9693985, "u": "kWh", "v": 120866.31},
{"n": "total_instantaneous_real_power", "t": 1647010680, "ut": 1647010678.9198678, "u": "kW", "v": 29.22}, 
{"n": "energy_phase_c", "t": 1647010680, "ut": 1647010678.9745023, "u": "kWh", "v": 19812.67}, 
{"n": "average_current", "t": 1647010680, "ut": 1647010678.9420502, "u": "A", "v": 48.43}, 
{"n": "apparent_power_c", "t": 1647010680, "ut": 1647010678.9360523, "u": "kVA", "v": 5.88},
{"n": "voltage_B", "t": 1647010680, "ut": 1647010678.9540834, "u": "V", "v": 239.64},
{"n": "frequency", "t": 1647010680, "ut": 1647010678.9502673, "u": "Hz", "v": 49.87},
{"n": "voltage_AB", "t": 1647010680, "ut": 1647010678.9581149, "u": "V", "v": 0.11}
]]


column_list=[]
name_list=[]
time_list=[]

def main():
    global count_data_insertion
    connection=create_connection()
    a = datetime.now()
    print(a)
    b=0
    while(1):
        cur = connection.cursor()
        count_data_insertion+=1
        for it in json_list:
            (name_list,column_list)=get_namelist_and_columnlist(it)
            try:
                create_Table(cur,column_list)
                connection.commit()
            except:
                print("Exception while creating the table and or Table Exist")
            try:
                insert_values(cur,name_list,column_list)
            except:
                print("Can't insert the values")
            # Exit_flag=True      # Uncomment it to make the exit condition depened on both list size and the time , commenting it makes it dependent on time and will repeat JSON list
        b=datetime.now()
        if float((b-a).total_seconds()) > TIME_IN_MINUTES or Exit_flag==True:#Exit flag controls the end of the file
            break
        system_usage_report(float((b-a).total_seconds()))
    create_report(count_data_insertion)
    plot_usage(ram_usage_gb,ram_percentage,cpu_usage,ram_time_plot)
    connection.commit()
    connection.close()


if __name__ == "__main__":
    main()



